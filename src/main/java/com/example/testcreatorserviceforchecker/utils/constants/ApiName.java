package com.example.testcreatorserviceforchecker.utils.constants;

import lombok.Getter;

@Getter
public class ApiName {

    private static final String Random = "/random";



    private static final String V1 = "/v1";                                      // for main
    private static final String Test = "/test";                                  // for main



    private static final String Word = "/word";
    private static final String Character = "/character";
    private static final String Number = "/number";
    private static final String PairNumber = "/pairnumber";
    private static final String PairCharacter = "/paircharacter";
    private static final String PairWord = "/pairword";



    private static final String Value = "/value";                                // for number
    private static final String MinValue = "/minvalue";                          // for number
    private static final String MaxValue = "/maxvalue";                          // for number
    private static final String OnlyDigit = "/onlydigit";                        // for character
    private static final String OnlyLatinAlphabet = "/onlylatinalphabet";        // for character
    private static final String OptionalCharacter = "/optionalcharacter";        // for character




    /*** **************** Main ******************* ***/
    public String getV1() {
        return V1;
    }
    public String getTest() {
        return Test;
    }


    /*** ***************** Get ******************** ***/
    public String Word() {
        return Word;
    }
    public String Character() {
        return Character;
    }
    public String Numberr(){
        return Number;
    }


    /*** **************** Random ****************** ***/
    public String getRandom() {
        return Random;
    }


    /*** **************** Value ******************* ***/
    public String Value() {                         // 1
        return Value;
    }
    public String MinValue() {                      // 2
        return MinValue;
    }
    public String MaxValue() {                      // 3
        return MaxValue;
    }


    /*** **************** Only ******************** ***/
    public String OnlyDigit() {                     // 1
        return OnlyDigit;
    }
    public String getOnlyLatinAlphabet() {          // 2
        return OnlyLatinAlphabet;
    }
    public String getOptionalCharacter() {          // 3
        return OptionalCharacter;
    }


    /*** **************** Pair ******************** ***/
    public String PairNumber() {                    // 1
        return PairNumber;
    }
    public String PairCharacter() {                 // 2
        return PairCharacter;
    }
    public String PairWord() {                      // 3
        return PairWord;
    }





}
