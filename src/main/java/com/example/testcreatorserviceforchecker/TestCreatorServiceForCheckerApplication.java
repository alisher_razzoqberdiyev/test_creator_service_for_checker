package com.example.testcreatorserviceforchecker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCreatorServiceForCheckerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestCreatorServiceForCheckerApplication.class, args);
    }

}
